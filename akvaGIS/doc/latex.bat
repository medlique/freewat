REM OSGeo4W should be installed. Note that this is not the one by default installed with QGIS
SET OSGEO_PATH=C:\PROGRA~1\QGIS21~1.4

SET SPHINXBUILDPATH=%OSGEO_PATH%\apps\Python27\Scripts
SET SPHINXBUILD=%OSGEO_PATH%\apps\Python27\Scripts\sphinx-build.exe
SET PATH=%PATH%;%SPHINXBUILDPATH%
SET PATH=%PATH%;%OSGEO_PATH%\bin
SET PATH=%PATH%;%OSGEO_PATH%\apps\qgis-ltr\bin
SET PATH=%PATH%;%programfiles(x86)%\Graphviz2.38\bin
SET PATH=%PATH%;%UserProfile%\.qgis2\python\plugins\akvagis
SET PYTHONHOME=%OSGEO_PATH%\apps\Python27

SET PYTHONPATH=%PYTHONPATH%;%UserProfile%\.qgis2\python\plugins\akvagis
SET PYTHONPATH=%PYTHONPATH%;%UserProfile%\.qgis2\python\plugins\akvagis\external;
SET PYTHONPATH=%PYTHONPATH%;%OSGEO_PATH%\apps\qgis-ltr\python\qgis
SET PYTHONPATH=%PYTHONPATH%;%OSGEO_PATH%\apps\qgis-ltr\python
SET PYTHONPATH=%PYTHONPATH%;%OSGEO_PATH%\apps\qgis-ltr\bin
SET PYTHONPATH=%PYTHONPATH%;%OSGEO_PATH%\apps\qgis-ltr
SET PYTHONPATH=%PYTHONPATH%;%OSGEO_PATH%\apps
SET PYTHONPATH=%PYTHONPATH%;%OSGEO_PATH%
SET PYTHONPATH=%PYTHONPATH%;%OSGEO_PATH%\bin

call make latex