view package
============

Submodules
----------

view.CheckBoxDelegate module
----------------------------

.. automodule:: view.CheckBoxDelegate
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: view
    :members:
    :undoc-members:
    :show-inheritance:
