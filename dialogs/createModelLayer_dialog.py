# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2014 - 2015 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************


from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
import os
from PyQt4 import QtGui, uic
from freewat.freewat_utils import getVectorLayerByName, getVectorLayerNames, getModelsInfoLists, getModelInfoByName
from freewat.mdoCreate_utils  import createModelLayer
from freewat.createGrid_utils import get_rgrid_nrow_ncol
#
#
FORM_CLASS, _ = uic.loadUiType(os.path.join( os.path.dirname(__file__), 'ui/ui_createModelLayer.ui') )
#
#class CreateModelLayerDialog(QDialog, Ui_CreateModelLayerDialog):
class CreateModelLayerDialog(QDialog, FORM_CLASS):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)

        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.createMLayer)
        # self.cancelButton.clicked.connect(self.stopProcessing)
        self.manageGui()
##
##
    def manageGui(self):
        self.cmbGridLayer.clear()
        self.cmbModelName.clear()
        layerNameList = getVectorLayerNames()

        layerNameList.sort()

        (modelNameList, pathList) =  getModelsInfoLists(layerNameList)

        self.cmbModelName.addItems(modelNameList)
        self.cmbLayType.addItems(['confined','convertible'])
        self.cmbLayavg.addItems(['harmonic','logarithmic', 'arithmetic-mean'])
        self.cmbLaywet.addItems(['No','Yes'])

        # fill the Grid combobox with only the _grid layer
        grid_layers = []
        for nametemp in layerNameList:
            if '_grid' in nametemp:
                grid_layers.append(nametemp)

        self.cmbGridLayer.addItems(grid_layers)


    def reject(self):
        QDialog.reject(self)


    def createMLayer(self, modeltable):

        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(0)
        self.progressBar.setValue(0)

        # ------------ Load input data  ------------
        modelName = self.cmbModelName.currentText()
        #
        gridLayer = getVectorLayerByName(self.cmbGridLayer.currentText())
        # ------------ Model geometry ------------

        # Load model layer:
        layerName =  self.txtLayerName.text()
        top = self.txtTop.text()
        bottom = self.txtBottom.text()

        # Values for LPF table
        #layType     = 'convertible'
        layType     = self.cmbLayType.currentText()
        layAverage  = self.cmbLayavg.currentText()  #'harmonic'
        layWet      = self.cmbLaywet.currentText() #'off'

        # REMARK: Default value for CHANI is 1

        chani   = 1.0
        # Define values of new feature for LPF

        ft = [layerName, layType,layAverage,chani,layWet]
        # --
        parameters = ft
        parameters.append(float(top))
        parameters.append(float(bottom))

       # Remark: pathfile from model table and number of stress periods (nsp) from time table
        (pathfile, nsp ) = getModelInfoByName(modelName)
        # Create a Model Layer

        createModelLayer(gridLayer, pathfile, modelName, layerName, parameters)

        # Retrieve the new layer from Map and insert correct values for BORDER.
        # BORDER =1   if cell is on a border line
        # else
        newlay = getVectorLayerByName(layerName)

        # Selection
        nrow, ncol =  get_rgrid_nrow_ncol(newlay)
        fieldIdx = newlay.dataProvider().fieldNameIndex('BORDER')

        newlay.startEditing()
        attributesDict = {}
        value = {fieldIdx : 1}

        for ft in newlay.getFeatures():
            if ft['COL'] == 1 or ft['COL'] == ncol or ft['ROW'] == 1 or ft['ROW'] == nrow :
                attributesDict[ft.id()] = value

        newlay.dataProvider().changeAttributeValues(attributesDict)
        newlay.commitChanges()


        self.progressBar.setMaximum(100)

        #Close the dialog window after the execution of the algorithm
        QDialog.reject(self)
##
