# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2014 - 2015 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************
from math import exp
from crop_utils import *

#VARIABLES
t_min = [7,2,8,4,10,6]
t_max = [2,5,8,10,6,9]
t_base = 4.0

lai_max = 3.5
gdd_em = 2.3
alpha_1 = 3.0
alpha_2 = 4.0
gdd_lai_max = 5.0

rad = [2.1,5.4,4,1.2,3.6,8.7]
rue = 1.7
hi_ref = 0.47

tr_act = [1.8,2.6,5.5,2.9,1.6,4.7]
tr_max = [2.4,2.5,0.8,9.7,4.5,6.3]
ky = 0.05


### ----- Phenology ------###
#media temperatura

c = CropGrowthModule(t_min, t_max, t_base, lai_max, gdd_em, alpha_1, alpha_2, gdd_lai_max, rad, rue, hi_ref, tr_act, tr_max, ky )

c.growing_degree_days()
